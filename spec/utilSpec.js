import Util from "../index";

describe("Util", function () {
  it("getStopLossPrice should return 8.08", function () {
    // Given
    const entryPrice = 8.6;
    const stopLossPriceMultiplier = 0.94;

    // When
    const stopLossPrice = Util.getStopLossPrice(entryPrice, stopLossPriceMultiplier);

    // Then
    expect(stopLossPrice).toBe(8.084);
  });

  it("getPositionSizeToBuy should return 142.85", function () {
    // Given
    const riskPerTrade = 0.01;
    const capital = 10000;
    const entryPrice = 8.6;
    const stopLossPrice = 7.9;

    // When
    const units = Util.getPositionSizeToBuy(riskPerTrade, capital, entryPrice, stopLossPrice);

    // Then
    expect(units).toBe(142.857142857143);
  });

  it("getTargetPrice should return 93", function () {
    // Given
    const entryPrice = 86;
    const stopLossPrice = 79;
    const targetMultiplier = 1;
    const currentPrice = 90;

    // When
    const targetPrice = Util.getTargetPrice(entryPrice, stopLossPrice, targetMultiplier, currentPrice);

    // Then
    expect(targetPrice).toBe(93);
  });

  it("getTargetPrice should return 100", function () {
    // Given
    const entryPrice = 86;
    const stopLossPrice = 79;
    const targetMultiplier = 1;
    const currentPrice = 94;

    // When
    const targetPrice = Util.getTargetPrice(entryPrice, stopLossPrice, targetMultiplier, currentPrice);

    // Then
    expect(targetPrice).toBe(100);
  });

  it("getPrecisionAdjustedEntryPrice should return 1.1 when entryPrice is 1.12 and precision is 1", function () {
    // Given
    const entryPrice = 1.12;
    const precision = 1;

    // When
    const adjustedPrice = Util.getDecimalPrecisionAdjustedEntryPrice(entryPrice, precision);

    // Then
    expect(adjustedPrice).toBe(1.1);
  });

  it("getDecimalPrecisionAdjustedEntryPrice should return 1.168 when entryPrice is 1.1679 and precision is 3",
    function () {
      // Given
      const entryPrice = 1.1679;
      const precision = 3;

      // When
      const adjustedPrice = Util.getDecimalPrecisionAdjustedEntryPrice(entryPrice, precision);

      // Then
      expect(adjustedPrice).toBe(1.168);
    }
  );
});