import moment from "moment-timezone";

function getLastXCandles(candles, x) {
  if (x) {
    return candles.slice(candles.length - x);
  }
}

function getCurrentTime() {
  return moment();
}

function getCurrentTimeFormatted(targetTimezone, targetFormat) {
  return moment(getCurrentTime()).tz(targetTimezone).format(targetFormat);
}

function getLowestMultipleOf100(amount) {
  let count = 0;
  while (amount / 100 >= 1) {
    amount = amount / 100;
    count++;
  }
  return Math.floor(amount) * Math.pow(100, count);
}

function getTransactionFeeBuffer(purchaseAmount, transactionFee) {
  return transactionFee * purchaseAmount;
}

function getLengthOfArray(array) {
  let length = 0;
  for (let elem in array) {
    if (elem) {
      length++;
    }
  }

  return length;
}

function getStopLossPrice(entryPrice, stopLossPriceMultiplier) {
  const stopLossPrice = stopLossPriceMultiplier * entryPrice;
  return stopLossPrice;
}

function getTargetPrice(entryPrice, stopLossPrice, targetMultiplier, currentPrice) {
  let targetPrice = 0;
  const risk = entryPrice - stopLossPrice;
  do {
    targetPrice = entryPrice + targetMultiplier * risk;
    targetMultiplier++;
  } while (targetPrice < currentPrice);
  return targetPrice;
}

function getPositionSizeToBuy(riskPerTrade, totalCapital, entryPrice, stopLossPrice) {
  const riskPerUnit = entryPrice - stopLossPrice;
  const units = riskPerTrade * totalCapital / riskPerUnit;
  return units;
}

function getDecimalPrecisionAdjustedEntryPrice(entryPrice, precision) {
  return Number(entryPrice.toFixed(precision));
}

function getVolumeToBuy(availableCapital, totalCapital, entryPrice, transactionFee,
  stopLossPriceMultiplier, riskPerTrade) {
  const maxVol = (availableCapital -
    getTransactionFeeBuffer(availableCapital, transactionFee)) / entryPrice;
  const stopLossPrice = getStopLossPrice(entryPrice, stopLossPriceMultiplier);
  const sizedVol = getPositionSizeToBuy(riskPerTrade, totalCapital, entryPrice, stopLossPrice);
  return sizedVol > maxVol ? maxVol : sizedVol;
}

module.exports = {
  getLastXCandles,
  getCurrentTime,
  getCurrentTimeFormatted,
  getLowestMultipleOf100,
  getTransactionFeeBuffer,
  getLengthOfArray,
  getStopLossPrice,
  getPositionSizeToBuy,
  getTargetPrice,
  getDecimalPrecisionAdjustedEntryPrice,
  getVolumeToBuy,
};